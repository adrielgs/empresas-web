import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(public http: HttpClient) { }

  public async get(endpoint, options): Promise<any> {

    return await this.http.get(
      this.url(endpoint),
      options
    ).toPromise()
  }

  public async post(endpoint, data) {
    return await this.http.post(
      this.url(endpoint),
      data,
      { observe: 'response' }
    ).toPromise()
  }

  public url(endpoint) {
    return `${environment.api}${endpoint}`
  }
}
