import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EnterprisesComponent } from './enterprises/enterprises.component';
import { EnterpriseDetailComponent } from './enterprises/enterprise-detail/enterprise-detail.component';
import { EnterpriseListComponent } from './enterprises/enterprise-list/enterprise-list.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'enterprises',
    component: EnterprisesComponent,
    children: [
      {
        path: '',
        component: EnterpriseListComponent
      },
      {
        path: ':id',
        component: EnterpriseDetailComponent
      }
    ]
  },
  {
    path: '**',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
