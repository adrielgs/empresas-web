import { Component, OnInit } from '@angular/core';
import { EnterprisesService } from './enterprises.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-enterprises',
  templateUrl: './enterprises.component.pug',
  styleUrls: ['./enterprises.component.styl']
})
export class EnterprisesComponent implements OnInit {

  private enterprises
  private name
  private enterpriseTypes
  public type = '0'

  constructor(private enterprisesService: EnterprisesService, private router: Router) { }

  async ngOnInit() {
    this.enterpriseTypes = await this.enterprisesService.getTypes()
  }

  private async search() {
    let params = {}

    this.name ? params['name'] = this.name : null
    if(!this.type || this.type.toString() !== '0') {
      params['enterprise_types'] = this.type
    }

    await this.enterprisesService.search(params)
    this.router.navigate(['enterprises'])
  }

}
