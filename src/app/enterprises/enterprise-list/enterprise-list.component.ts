import { Component, OnInit } from '@angular/core';
import { EnterprisesService } from '../enterprises.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-enterprise-list',
  templateUrl: './enterprise-list.component.pug',
  styleUrls: ['./enterprise-list.component.styl']
})
export class EnterpriseListComponent implements OnInit {

  private enterprises = []
  constructor(private enterprisesService: EnterprisesService, private router: Router) { }

  ngOnInit() {
    this.enterprises = this.enterprisesService.getEnterprises()
  }

  private seeDetails(id) {
    this.router.navigate([`enterprises/${id}`])
  }
}
