import { Injectable } from '@angular/core';
import { ApiService } from '../common/services/api.service'

@Injectable({
  providedIn: 'root'
})
export class EnterprisesService {

  public enterprises

  constructor(private api: ApiService) { }

  public async search(params) {
    const options = {
      headers: this.buildHeaders(),
      params
    }
    this.enterprises = (await this.api.get('enterprises', options)).enterprises
  }

  public async getById(id) {
    return await this.api.get(`enterprises/${id}`, { headers: this.buildHeaders() })
  }

  public async getTypes() {
    const enterprises = (await this.api.get(`enterprises`, { headers: this.buildHeaders() })).enterprises

    let enterpriseType = enterprises.map((enterprise) => enterprise.enterprise_type)
    const uniqueType = [...new Set(enterpriseType.map(type => type.enterprise_type_name))]
    uniqueType.sort()
    let uniqueEnterpriseType = []
    uniqueType.forEach(enterprise_type_name => {
      uniqueEnterpriseType.push(enterpriseType.find(type => type.enterprise_type_name === enterprise_type_name))
    })

    return uniqueEnterpriseType

  }

  private buildHeaders() {
    return {
      'access-token': localStorage.getItem('access-token'),
      'client': localStorage.getItem('client'),
      'uid': localStorage.getItem('uid')
    }
  }

  public getEnterprises() {
    return this.enterprises
  }
}
