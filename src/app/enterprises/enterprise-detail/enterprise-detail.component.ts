import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EnterprisesService } from '../enterprises.service'

@Component({
  selector: 'app-enterprise-detail',
  templateUrl: './enterprise-detail.component.pug',
  styleUrls: ['./enterprise-detail.component.styl']
})
export class EnterpriseDetailComponent implements OnInit {

  private enterprise
  constructor(private activatedRoute: ActivatedRoute, private enterprisesService: EnterprisesService) { }

  async ngOnInit() {
    const id = this.activatedRoute.snapshot.paramMap.get('id')
    this.enterprise = (await this.enterprisesService.getById(id)).enterprise
  }


}
