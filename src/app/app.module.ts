import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EnterprisesComponent } from './enterprises/enterprises.component';

import {HttpClientModule} from '@angular/common/http';
import { EnterpriseDetailComponent } from './enterprises/enterprise-detail/enterprise-detail.component';
import { EnterpriseListComponent } from './enterprises/enterprise-list/enterprise-list.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EnterprisesComponent,
    EnterpriseDetailComponent,
    EnterpriseListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
