import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.pug',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {

  private email
  private password

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  private async login(loginForm) {
    if(loginForm.valid) {
      try {
        await this.loginService.login(this.email, this.password)
        this.router.navigate(['enterprises'])
        // TODO wrong credentials
      } catch (error) {
        console.log(error)
      }
    }
  }

}
