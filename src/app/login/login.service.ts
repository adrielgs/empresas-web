import { Injectable } from '@angular/core';
import { ApiService } from '../common/services/api.service'
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private api: ApiService) { }

  public async login(email, password) {
    const response = await this.api.post('users/auth/sign_in', {email, password})
    localStorage.setItem('access-token', response.headers.get('access-token'))
    localStorage.setItem('client', response.headers.get('client'))
    localStorage.setItem('uid', response.headers.get('uid'))
  }
}
